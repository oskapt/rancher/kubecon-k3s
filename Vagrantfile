# -*- mode: ruby -*-
# vi: set ft=ruby :
require 'ipaddr'
require 'yaml'

x = YAML.load_file('config.yaml')
# puts "Config: #{x.inspect}\n\n"

$private_nic_type = x.fetch('net').fetch('private_nic_type')

# --- Check for missing plugins
required_plugins = %w( vagrant-timezone vagrant-reload )
plugin_installed = false
required_plugins.each do |plugin|
  unless Vagrant.has_plugin?(plugin)
    system "vagrant plugin install #{plugin}"
    plugin_installed = true
  end
end
# --- If new plugins installed, restart Vagrant process
if plugin_installed === true
  exec "vagrant #{ARGV.join' '}"
end

Vagrant.configure(2) do |config|
  config.vm.define "master" do |master|
    c = x.fetch('master')
    master.vm.box = "chenhan/lubuntu-desktop-18.04"
    master.vm.box_version = "20180704.0.0"
    master.vm.provider :virtualbox do |v|
      v.gui = c.fetch('start_gui')
      v.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
      v.cpus = c.fetch('cpus')
      v.linked_clone = true if Gem::Version.new(Vagrant::VERSION) >= Gem::Version.new('1.8.0') and x.fetch('linked_clones')
      v.memory = c.fetch('memory')
      v.name = "master"
    end
    master.vm.network x.fetch('net').fetch('network_type'), ip: x.fetch('ip').fetch('master') , nic_type: $private_nic_type
    master.vm.hostname = "master"
    master.vm.provision "bootstrap", type: "shell" do |s|
      ssh_pub_key = File.readlines("#{Dir.home}/.ssh/#{x.fetch('ssh_public_key')}").first.strip
      s.inline = <<-SHELL
        echo "Installing NTP"
        apt-get -qq update && apt-get -qq install ntp
        echo "Configuring SSH"
        echo #{ssh_pub_key} >> /home/vagrant/.ssh/authorized_keys
        chmod 644 /home/vagrant/.ssh/authorized_keys
        mkdir -p /root/.ssh
        chmod 700 /root/.ssh
        echo #{ssh_pub_key} >> /root/.ssh/authorized_keys
        chmod 644 /root/.ssh/authorized_keys
        echo "Downloading k3s v#{x.fetch('k3s_version')}"
        curl -sfL https://github.com/rancher/k3s/releases/download/v#{x.fetch('k3s_version')}/k3s -o /usr/local/bin/k3s
        chmod 0755 /usr/local/bin/k3s
      SHELL
    end
    master.vm.provision "install-script", type: "file", source: "scripts/deploy-k3s.sh", destination: "deploy-k3s.sh"
    master.vm.provision "install-demo", type: "file", source: "demo", destination: "demo"
    master.vm.provision "configure-script", type: "shell" do |s|
      s.inline = <<-SHELL
        echo "Patching k3s deploy script"
        sed -i'' -e 's/%%K3S_VERSION%%/#{x.fetch('k3s_version')}/' deploy-k3s.sh
        chmod 755 deploy-k3s.sh
      SHELL
    end
    master.vm.provision :reload
  end

  worker_ip = IPAddr.new(x.fetch('ip').fetch('worker'))
  (1..x.fetch('worker').fetch('count')).each do |i|
    c = x.fetch('worker')
    hostname = "worker%02d" % i
    config.vm.define hostname do |worker|
      worker.vm.box = "chenhan/lubuntu-desktop-18.04"
      worker.vm.box_version = "20180704.0.0"
      worker.vm.provider "virtualbox" do |v|
        v.gui = c.fetch('start_gui')
        v.cpus = c.fetch('cpus')
        v.linked_clone = true if Gem::Version.new(Vagrant::VERSION) >= Gem::Version.new('1.8.0') and x.fetch('linked_clones')
        v.memory = c.fetch('memory')
        v.name = hostname
      end
      worker.vm.network x.fetch('net').fetch('network_type'), ip: IPAddr.new(worker_ip.to_i + i - 1, Socket::AF_INET).to_s, nic_type: $private_nic_type
      worker.vm.hostname = hostname
      worker.vm.provision "bootstrap", type: "shell" do |s|
        ssh_pub_key = File.readlines("#{Dir.home}/.ssh/#{x.fetch('ssh_public_key')}").first.strip
        s.inline = <<-SHELL
          echo "Installing NTP"
          apt-get -qq update && apt-get -qq install ntp
          echo "Configuring SSH"
          echo #{ssh_pub_key} >> /home/vagrant/.ssh/authorized_keys
          chmod 644 /home/vagrant/.ssh/authorized_keys
          mkdir -p /root/.ssh
          chmod 700 /root/.ssh
          echo #{ssh_pub_key} >> /root/.ssh/authorized_keys
          chmod 644 /root/.ssh/authorized_keys
          echo "Downloading k3s v#{x.fetch('k3s_version')}"
          curl -sfL https://github.com/rancher/k3s/releases/download/v#{x.fetch('k3s_version')}/k3s -o /usr/local/bin/k3s
          chmod 0755 /usr/local/bin/k3s
        SHELL
      end
      worker.vm.provision "install-script", type: "file", source: "scripts/deploy-k3s.sh", destination: "deploy-k3s.sh"
      worker.vm.provision "configure-script", type: "shell" do |s|
        s.inline = <<-SHELL
          sed -i'' -e 's/%%K3S_VERSION%%/#{x.fetch('k3s_version')}/' deploy-k3s.sh
          chmod 755 deploy-k3s.sh
        SHELL
      end
      worker.vm.provision :reload
    end
  end

end
