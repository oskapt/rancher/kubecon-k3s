# Vagrant K3s Demo

Sample Demo available at <https://youtu.be/SFt-gyEVLyo>.

The code in this repo will bring up two nodes, each with 2 cores and 1GB of RAM, running Ubuntu 18.04. It will then proceed to:

- Install NTP (because vbox nodes will lose time after snapshot restores)
- Set up SSH for `root` and `vagrant`
- Install K3s (but not deploy it)
- Install a deploy script for K3s
- Install the demo environment from `./demo`
- Reload the system

## Setup

To install from nothing, run `vagrant up`. This will create a node called `master` and the number of worker nodes specified in `config.yaml`, starting with `worker01` and incrementing up.

The `master` node has the address 172.22.101.101. The `workerXX` nodes start at 172.22.101.111.

Systems can be reinstalled with `vagrant destroy` followed by `vagrant up`. This takes several minutes to complete and should only be done if absolutely necessary.

When the systems are brought up for the first time, and before the first installation of K3s, the nodes should have a snapshot taken:

```bash
vagrant snapshot push
```

To reset the environment between each demo, restore the snapshot:

```bash
vagrant snapshot pop --no-delete
```

**NOTE:** This will often hang at "waiting for machine to come up," when the machine has already come up. If it sticks at this message for more than a few seconds, reset the machine in the VirtualBox console without pressing Control-C in the terminal window. The machine will come up again in a few seconds, and Vagrant will detect it and continue.

## Provisioning a Cluster

Vagrant drops `deploy-k3s.sh` in the `vagrant` home directory. This script will adjust itself according to arguments - if none are provided, it will deploy the server. It will also accept two positional arguments, which will map to `$K3s_URL` and `$K3S_TOKEN`, and if these are provided, it will deploy the agent.

### Server

```bash
./deploy-k3s.sh
```

Once installed, if you wish to bring down the `k3s.yaml` file for your local use, you can do so with:

```bash
scp root@172.22.101.101:/etc/rancher/k3s/k3s.yaml kubeconfig
sed -i'' -e 's/127.0.0.1/172.22.101.101/' kubeconfig
export KUBECONFIG=$(pwd)/kubeconfig
```

A script to do this is available at `./scripts/fetch.sh`.

### Agent

The exact command to use is pasted at the completion of the server install. Copy it and paste it into the `worker01` window.

```bash
./deploy-k3s.sh $MASTER_URL $TOKEN
```

## Deploying a Workload

A simple Kustomize workload is found in `./demo` and can be installed with `kubectl apply -k demo`. This will create a ConfigMap, Deployment, Service, and an Ingress listening on <https://rancher-demo.default.172.22.101.111.xip.io>.

This directory is copied to the master, so it can be installed from the vagrant host or the master node with the same command.

The demo will install `monachus/rancher-demo`, which shows a colored cow for the number of replicas active in the deployment (default: 3). If you wish to show changes to the demo config, you can edit `kustomization.yaml` and change `COW_COLOR` in the `configMapGenerator` to any recognized CSS color, such as blue, yellow, pink, red, purple, black, white, green, and others. You can also change the number of replicas.

After changing the config, run `kubectl apply -k demo` to apply the changes to the cluster.

## Using k3sup

To show the use of `k3sup`, Vagrant will create an `authorized_keys` file that includes the provided SSH public key (`id_rsa.pub` by default) from your local SSH directory. You can then use `k3sup` immediately after bringing up the systems.

If you wish to use a different SSH key, specify it in `config.yaml`.

### Server

```bash
k3sup install --ip=172.22.101.101 --user=root --k3s-extra-args '--flannel-iface eth1' --k3s-version=v0.10.2
```

This will save the `kubeconfig` file in the local directory.

### Agent

```bash
k3sup join --ip=172.22.101.111 --user=root --server-ip=172.22.101.101 --k3s-extra-args '--flannel-iface eth1' --k3s-version=0.10.2
```

## Demo

### Key Points

- show low-power VMs (2 core, 1G RAM) with nothing on it (htop)
- install k3s on master
- install k3s on worker01
- show that there's still 512M RAM available
- launch `monachus/rancher-demo` w/ service and ingress
- show it in a browser
- mention k3sup
- restore VMs from snapshot to clear out install
