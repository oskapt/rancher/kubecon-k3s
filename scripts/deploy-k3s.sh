#!/bin/bash

INT=eth1

export K3S_URL=$1
export K3S_TOKEN=$2
export INSTALL_K3S_EXEC="--flannel-iface=${INT}"
export INSTALL_K3S_VERSION=%%K3S_VERSION%%
export INSTALL_K3S_SKIP_DOWNLOAD=true

curl -sfL -o installer.sh https://get.k3s.io
chmod 0755 installer.sh
./installer.sh

# prep images for demo
echo "Pre-loading demo image"
sleep 5
sudo crictl pull monachus/rancher-demo:latest

if [[ -z ${K3S_URL} ]]; then
    # server
    MY_IP=$(ip a sh ${INT} | grep inet | grep ${INT} | awk '{ print $2 }' | awk -F/ '{ print $1 }')
    K3S_URL="https://${MY_IP}:6443"
    K3S_TOKEN=$(sudo cat /var/lib/rancher/k3s/server/node-token)
    echo "Run the following on other nodes:"
    echo "  ./deploy-k3s.sh \"${K3S_URL}\" \"${K3S_TOKEN}\""
fi