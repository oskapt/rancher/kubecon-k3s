#!/bin/bash

scp root@172.22.101.101:/etc/rancher/k3s/k3s.yaml kubeconfig
sed -i'' -e 's/127.0.0.1/172.22.101.101/' kubeconfig

cat<<EOT
For Bash users:

  export KUBECONFIG=$(pwd)/kubeconfig

For Fish users:

  set -x KUBECONFIG (pwd)/kubeconfig
EOT